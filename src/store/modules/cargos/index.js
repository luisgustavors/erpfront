
import axios from "axios"
import{API_VERSION, TOKEN_NAME} from "@/config"

export default
    {
        state: {
            cargos: []
        },
        mutations: {
            SET_CARGOS(state, cargo) {
                state.cargos = Object.assign({}, state.cargos, cargo)
            }
        },
        actions: {
            getCARGOS({ commit }) {
                return axios.get('v1/cargos', {
                    headers: {
                        Authorization: 'Bearer ' + localStorage.getItem(token)
                    }
                })
                    .then(response => {
                        commit('SET_CARGOS', response.data.data)
                    })
            },
        },
        getters: {
            getCargs(state) { return state.cargos }
        },
    }
